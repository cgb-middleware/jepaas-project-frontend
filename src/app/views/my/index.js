import router from './router';
import config from './config.json';
import install from '../../util/install';

install(router, config);
